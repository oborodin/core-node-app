import { BookModel } from '../../domain/book/book.model';

export class BookEntity {
  public id: number = 0;
  public name: string = '';

  public static mapToModel(book: BookEntity): BookModel {
    return {
      id: book.id,
      name: book.name,
    };
  }

  public static mapFromModel(book: BookModel): BookEntity {
    return {
      id: book.id,
      name: book.name,
    };
  }
}
