import { BookConstRepository } from '../../entity/const/book.const.repository';
import { BookRepositoryInterface } from './book.repository.interface';
import { BookService } from './book.service';

describe('BookService', (): void => {
  let bookService: BookService;

  it('#getAll', () => {
    const repository: BookRepositoryInterface = new BookConstRepository();

    const getAllSpy: jasmine.Spy = spyOn(repository, 'getAll');

    bookService = new BookService(repository);

    bookService.getAll();

    expect(getAllSpy).toHaveBeenCalled();
  });

  it('#getId', () => {
    const repository: BookRepositoryInterface = new BookConstRepository();

    const getIdSpy: jasmine.Spy = spyOn(repository, 'getId');

    bookService = new BookService(repository);

    bookService.getId(1);

    expect(getIdSpy).toHaveBeenCalledWith(1);
  });
});
