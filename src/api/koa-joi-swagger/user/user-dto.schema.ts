import { array, ArraySchema, number, object, ObjectSchema, string } from 'joi';
import { definition } from 'koa-joi-swagger-ts';

import { BaseResponseSchema } from '../shared/base-response.schema';
import { dto, DtoSchema } from '../shared/dto.schema';
import { UserDto } from './user.dto';

const EXAMPLE_USER: UserDto = {
  id: 5,
  name: 'user5',
};

export const userDtoSchema: DtoSchema<UserDto> = dto<UserDto>().keys({
  id: number()
    .integer()
    .positive()
    .description('User ID')
    .example(EXAMPLE_USER.id)
    .required(),
  name: string()
    .description('User name')
    .example(EXAMPLE_USER.name)
    .required(),
});

@definition('UserResponse', 'Object of user')
export class UserResponseSchema extends BaseResponseSchema {
  public data: ObjectSchema = userDtoSchema.required();
}

@definition('UsersResponse', 'Array of users')
export class UsersResponseSchema extends BaseResponseSchema {
  public data: ArraySchema = array()
    .items(userDtoSchema)
    .required();
}
