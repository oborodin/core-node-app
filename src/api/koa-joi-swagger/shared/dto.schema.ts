import { object, ObjectSchema, SchemaLike } from 'joi';

export interface DtoSchema<T> extends ObjectSchema {
  keys(
    schema?: {
      [key in keyof T]: SchemaLike | SchemaLike[];
    }
  ): this;
}

export function dto<T>(): DtoSchema<T> {
  return object();
}
