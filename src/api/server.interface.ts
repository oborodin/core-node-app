export interface Server {
  listen(port: number | string): Promise<void>;
}
