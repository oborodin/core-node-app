import { BookModel } from '../../../domain/book/book.model';

export class BookDto {
  public id: number = 0;
  public name: string = '';

  public static mapToModel(book: BookDto): BookModel {
    return {
      id: book.id,
      name: book.name,
    };
  }

  public static mapFromModel(book: BookModel): BookDto {
    return {
      id: book.id,
      name: book.name,
    };
  }
}
