import * as bodyParser from 'koa-bodyparser';
import { RouterContext } from 'koa-router';
import Router = require('koa-router');

import { BookModel } from '../../../domain/book/book.model';
import { BookService } from '../../../domain/book/book.service';
import { BookConstRepository } from '../../../entity/const/book.const.repository';

import { HttpStatusEnum } from '../../http-status.enum';

import { BookDto } from './book.dto';

export class BookRouter {
  public path: string;
  private router: Router = new Router();
  private service: BookService;

  constructor(path: string) {
    this.path = path;

    this.service = new BookService(new BookConstRepository());
    this.router = this.initRouter(this.path, this.service);
  }

  private initRouter(route: string, service: BookService): Router {
    const router: Router = new Router();

    router.get(route, async (ctx: RouterContext) => {
      (ctx.body as BookDto[]) = service.getAll().map(BookDto.mapFromModel);
      return;
    });

    router.get(`${route}/:id`, async (ctx: RouterContext) => {
      const book: BookModel | undefined = service.getId(+ctx.params.id);

      if (book) {
        (ctx.body as BookDto) = BookDto.mapToModel(book);
      } else {
        ctx.status = HttpStatusEnum.NotFound;
      }
      return;
    });

    router.post(route, bodyParser(), async (ctx: RouterContext) => {
      const createBook: BookDto = ctx.request.body as BookDto;

      ctx.body = BookDto.mapFromModel(service.create(createBook));
      return;
    });

    router.put(`${route}/:id`, bodyParser(), async (ctx: RouterContext) => {
      const book: BookModel | undefined = service.update(
        +ctx.params.id,
        BookDto.mapFromModel(ctx.request.body as BookDto)
      );

      if (book) {
        (ctx.body as BookDto) = BookDto.mapToModel(book);
      } else {
        ctx.status = HttpStatusEnum.NotFound;
      }

      return;
    });

    router.delete(`${route}/:id`, async (ctx: RouterContext) => {
      (ctx.body as boolean) = service.delete(+ctx.params.id);

      return;
    });

    return router;
  }

  public getRouters(): Router.IMiddleware {
    return this.router.routes();
  }
}
