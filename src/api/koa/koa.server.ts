import * as Koa from 'koa';
import * as logger from 'koa-logger';
import * as Router from 'koa-router';

import { Server } from '../server.interface';
import { BookRouter } from './book/book.router';

export class KoaServer implements Server {
  /**
   * singleton экземплюр Server
   */
  private static instance: KoaServer | undefined = undefined;

  private application: Koa;

  /**
   * Получение singleton экземпляра класса
   */
  public static getInstance(): KoaServer {
    if (KoaServer.instance === undefined) {
      KoaServer.instance = new KoaServer();
    }
    return KoaServer.instance;
  }
  private constructor() {
    this.application = new Koa();

    const router: Router = this.initRouters();
    this.application.use(logger());
    this.application.use(router.routes());
    this.application.use(router.allowedMethods());
  }

  private initRouters(): Router {
    const router: Router = new Router();
    router.use(new BookRouter('/book').getRouters());
    return router;
  }

  /**
   * Поднятие сервера
   * @param port Порт на котором будет поднял сервер
   */
  public async listen(port: number | string): Promise<void> {
    return new Promise<void>((resolve: () => void, reject: (reason?: string) => void): void => {
      this.application
        .listen(port, () => {
          resolve();
        })
        .on('error', (err: string) => {
          reject(err);
        });
    });
  }
}
