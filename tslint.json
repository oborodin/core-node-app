{
  "rulesDirectory": ["tslint-consistent-codestyle"],
  "rules": {
    // Последовательная перегрузка
    "adjacent-overload-signatures": true,
    // Запрет игнорирования линта
    "ban-ts-ignore": true,
    // Запрет на использование типов по регулярным выражениям
    "ban-types": [true, ["Object", "Use {} instead."], ["String"]],
    // Обязательное указание области видимости
    "member-access": true,
    // Запрет на использование any
    "no-any": true,
    // Запрет на пустые интерфейсы
    "no-empty-interface": true,
    // Запрет на использование for-in
    "no-for-in": true,
    // Запрет на импорт с побочными эффектами
    "no-import-side-effect": true,
    // Запрет внутренних модулей
    "no-internal-module": true,
    // Запрет магических чисел
    "no-magic-numbers": [true],
    // Запрет использования пространства имен
    "no-namespace": true,
    // Запрещает ненулевых утверждений
    "no-non-null-assertion": true,
    // Запрет импорта через require
    "no-var-requires": true,
    // Рекомендация использования for-of вместо for
    "prefer-for-of": true,
    // Требование установки async для () => Promise
    "promise-function-async": true,
    // Обязательные вызвращаемые типы
    "typedef": {
      "severity": "error",
      "options": [
        "call-signature",
        "arrow-call-signature",
        "parameter",
        "arrow-parameter",
        "property-declaration",
        "variable-declaration",
        "member-variable-declaration",
        "object-destructuring",
        "array-destructuring"
      ]
    },
    // Предупреждение о возможности создания перегрузки
    "unified-signatures": true,
    /*  Functionality  */
    // Предупреждение об отсутствии Promise после await
    "await-promise": true,
    // Запрет на использование запятых в операторах
    "ban-comma-operator": true,
    // Запрет использования спецсимолов и глобальных переменных
    "ban": [true, "eval"],
    // Обязательные {} после if
    "curly": true,
    // Запрет на использование конструктора функций
    "function-constructor": true,
    // Запрет импортов по черному листу
    "import-blacklist": [true, "rxjs/Rx"],
    // Запрет использования label
    "label-position": true,
    // Запрет на использование arguments.callee
    "no-arg": true,
    // Запрет на async без await
    "no-async-without-await": true,
    // Запрет на использование побитовых операций
    "no-bitwise": true,
    // Запрет на "=" в операторах do-while, for, if, и while
    "no-conditional-assignment": true,
    // Запрет вывода в консоль
    "no-console": [true, "debug", "info", "time", "timeEnd", "trace"],
    // Запрет конструктора String, Number, и Boolean
    "no-construct": true,
    // Запрет debugger
    "no-debugger": true,
    // Запрет дублирования super в конструкторе
    "no-duplicate-super": true,
    // Запрет дублирования case в switch
    "no-duplicate-switch-case": true,
    // Запрет дублирования переменных в 1й области видимости
    "no-duplicate-variable": true,
    // Запрет на динамическое удаление полей
    "no-dynamic-delete": true,
    // Запрет на пустые блоки
    "no-empty": true,
    // Запрет на eval
    "no-eval": true,
    // Запрет на не обработанный Promise
    "no-floating-promises": true,
    // Запрет for-in по массиву
    "no-for-in-array": true,
    // Запрет импорта зависимостей мимо package.json
    "no-implicit-dependencies": [true, ["@domain", "@api", "@entity"]],
    // Запрет типа {}
    "no-inferred-empty-object-type": true,
    // Запрет ${ не в шаблонных строках
    "no-invalid-template-strings": true,
    // Запрет на использование this вне классов
    "no-invalid-this": true,
    // Запрет переопределения конструкторов
    "no-misused-new": true,
    // Запрет использования объектных литералов без типа
    "no-object-literal-type-assertion": true,
    // Запрет на использование глобальных переменных
    "no-restricted-globals": [true, "name", "length", "event"],
    // Запрет на использование "return await"
    "no-return-await": true,
    // Запрет на теневое переопределение переменных
    "no-shadowed-variable": true,
    // Запрет литерали массива с пустым значением
    "no-sparse-arrays": true,
    // Запрет на обращение к свойствам объекта через строку object['...']
    "no-string-literal": true,
    // Запрет проваливая в case после функционала
    "no-switch-case-fall-through": true,
    // Запрет сравнения со всегда верным исходом (3 === 3)
    "no-tautology-expression": true,
    // Запрет созданий ссылок на this (const self = this;)
    "no-this-assignment": true,
    // Запрет на использование не используемых классов
    "no-unnecessary-class": true,
    // Запрет на неиспользуемые выражения и операторы
    "no-unused-expression": true,
    // Запрет на неиспользуемые импорты
    "no-unused-variable": true,
    // Запрет на использование переменных до объявления
    "no-use-before-declare": true,
    // ? Рекомендует использовать условные выражения
    "prefer-conditional-expression": [true, "check-else-if"],
    // Предпочтительно spread для копирования
    "prefer-object-spread": true,
    // Обязательные типы при реструктуризации
    "restrict-plus-operands": true,
    // Запрет на использование this в статических методах
    "static-this": true,
    // Сравнение через ===
    "triple-equals": true,
    // Проверка при сравнении с typeof
    "typeof-compare": true,
    // Запрет на пустой конструктор
    "unnecessary-constructor": true,
    /*  Maintainability*/
    // Ограничение на уровень цикломатийной сложности (вложенности) в функции
    "cyclomatic-complexity": [true, 3],
    // Предупреждение при использовании deprecation
    "deprecation": true,
    // Максимальное количество классов в файле
    "max-classes-per-file": [true, 3],
    // Максимальное количество строк в файле
    "max-file-line-count": [true, 300],
    // Запрет default export
    "no-default-export": true,
    // Запрет несколькоих строк импортов из 1го модуля
    "no-duplicate-imports": true,
    // Обязательно использование const при возможности
    "prefer-const": true,
    // Обязательно readonly при возможности
    "prefer-readonly": true,
    // Порядок членов класса
    "member-ordering": [
      true,
      "static-before-instance",
      "variables-before-functions",
      {
        "order": ["static-field", "instance-field", "static-method", "instance-method"]
      }
    ],
    /*  Style  */
    // Обязательное использование T[]
    "array-type": [true, "array"],
    // Короткий return из стрелочной функции при возможности
    "arrow-return-shorthand": true,
    // Литирал должен быть справа
    "binary-expression-operand-order": true,
    // Имена классов и интерфейсов в PascalCased
    "class-name": true,
    // Проверка форматирования для однострочных комментариев
    "comment-format": [true, "check-space", "check-uppercase"],
    // Тип для комментариев
    "comment-type": [true, "doc", "singleline"],
    // Проверка соглашения об именовании файлов
    "file-name-casing": [true, "kebab-case"],
    // Устанавливает приоритет интерфейсов над типами
    "interface-over-type-literal": true,
    // Обязательный перенос chained на следующию строку
    "newline-per-chained-call": false,
    // Устанавливает приоритет as над <> при указании типа
    "no-angle-bracket-type-assertion": true,
    // Запрет сравнения с логическими литералами
    "no-boolean-literal-compare": true,
    // Имена полей могут быть в "" только при необходимости
    "object-literal-key-quotes": [true, "as-needed"],
    // Проверка на нахождени в 1й строке операторов с } ...
    "one-line": [
      true,
      "check-open-brace",
      "check-catch",
      "check-finally",
      "check-else",
      "check-whitespace"
    ],
    // Запрет на множественное определение переменных
    "one-variable-per-declaration": true,
    // Импорты в алфавитном порядке
    "ordered-imports": true,
    // Использовани switch вместо if от 3х сравнений
    "prefer-switch": [
      true,
      {
        "min-cases": 3
      }
    ],
    // Использование шаблонных выражений вместо конкатинации
    "prefer-template": true,
    // Проверка имен переменных на наличие ошибок
    "variable-name": [
      true,
      "ban-keywords",
      "check-format",
      "allow-leading-underscore",
      "require-const-for-all-caps"
    ],
    // Использование интерфейсов без I
    "interface-name": [false, "always-prefix"],
    "callable-types": true,
    "import-spacing": true,
    /*  Format  */
    // Обязательные скобки в параметрах стрелочной функции
    "arrow-parens": true,
    // Файл должен оканчиваться пустой строкой
    "eofline": true,
    // Правила отступов
    "indent": [true, "spaces", 2],
    // Максимальная длинна строки
    "max-line-length": [
      true,
      {
        "limit": 100,
        "ignore-pattern": "^import"
      }
    ],
    // Запрет количества строк подряд
    "no-consecutive-blank-lines": [true, 2],
    // Запрет беспорядочных пробелов
    "no-irregular-whitespace": true,
    // Запрет на пробелы в конце строки
    "no-trailing-whitespace": true,
    // Устанавливает тип кавычек
    "quotemark": [true, "single"],
    // Обязательные ;
    "semicolon": [true, "always", "ignore-bound-class-methods"],
    // Использование зяпятых
    "trailing-comma": [
      false,
      {
        "multiline": "always",
        "singleline": "never"
      }
    ],
    // Отсутствие пробелов перед : в определении типа
    "typedef-whitespace": [
      true,
      {
        "call-signature": "nospace",
        "index-signature": "nospace",
        "parameter": "nospace",
        "property-declaration": "nospace",
        "variable-declaration": "nospace"
      }
    ],
    // Регулировка пробелов
    "whitespace": [
      true,
      "check-branch",
      "check-decl",
      "check-operator",
      "check-separator",
      "check-type"
    ],
    // Запрет на использование var
    "no-var-keyword": true,
    // Сортировка полей в объекте
    "object-literal-sort-keys": {
      "severity": "warn",
      "options": ["match-declaration-order"]
    },
    // Правила для jsdoc
    "jsdoc-format": [true, "check-multiline-start"]
  }
}
