# Core node app

This core structure for ts node application.

The app have 3 layers:

<ul>
    <li>
        API layer - for communication with client with help DTO
    </li>
     <li>
        Domain layer - for business rules. 
        The layer don't know about other layers and communication with other about "Model" 
        and use entity interface for revers dependence. 
    </li>
     <li>
        Entity layer - for saving data
    </li>
</ul>

<h2>Install typescript</h2>
<ul>
    <li>
        npm i --save-dev typescript ts-node nodemon
    </li>
     <li>
        add tsconfig.json - file settings for typescript
    </li>
</ul>

<h2>Install jasmine for testing</h2>
<ul>
    <li>
        npm i --save-dev jasmine @types/jasmine jasmine-console-reporter
    </li>
     <li>
        add jasmine.json - file settings for jasmine
    </li>
</ul>

<h2>Install lint and prettier</h2>
<ul>
    <li>
        npm i --save-dev tslint tslint-consistent-codestyle prettier
    </li>
    <li>
         add tslint.json - file settings for tslint
    </li>
     <li>
        add .prettierrc - file settings for prettier
    </li>
</ul>

<h2>Install koa as api server</h2>
<ul>
    <li>
        npm i koa koa-router
    </li>
    <li>
        npm i --save-dev @types/koa @types/koa-router
    </li>
</ul>

<h2>Install koa-joi-swagger-ts how second variant as api server. 
    The variant have joi validators and swagger auto generator</h2>
<ul>
    <li>
        npm i koa koa-router joi koa-joi-swagger-ts
    </li>
    <li>
        npm i --save-dev @types/koa @types/koa-router @types/joi
    </li>
</ul>
