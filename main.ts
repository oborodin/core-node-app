import { KoaJoiSwaggerServer } from './src/api/koa-joi-swagger/koa-joi-swagger.server';
import { KoaServer } from './src/api/koa/koa.server';
import { Server } from './src/api/server.interface';

const port: string = process.env.PORT || '3300';
const swaggerHost: string = `localhost:${port}`;

// const server: Server = KoaServer.getInstance();
const server: Server = KoaJoiSwaggerServer.getInstance(swaggerHost);

server
  .listen(port)
  .then(() => {
    console.log(`Server running on port ${port}`);
  })
  .catch((err: string) => {
    console.error(`Error server running on port ${port}: `, err);
  });
